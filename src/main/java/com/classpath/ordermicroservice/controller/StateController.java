package com.classpath.ordermicroservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.availability.*;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.boot.availability.LivenessState.*;
import static org.springframework.boot.availability.ReadinessState.ACCEPTING_TRAFFIC;
import static org.springframework.boot.availability.ReadinessState.REFUSING_TRAFFIC;

@RestController
@RequestMapping("/api/state")
@RequiredArgsConstructor
public class StateController {

    private final ApplicationAvailability applicationAvailability;
    private final ApplicationEventPublisher applicationEventPublisher;


    @PostMapping
    @RequestMapping("/liveness")
    public String updateLivenessState(){
        LivenessState currentLivenessState = this.applicationAvailability.getLivenessState();
        LivenessState updatedLivenessState = currentLivenessState == CORRECT ? BROKEN : CORRECT;
        String state = updatedLivenessState == CORRECT ? "Application is functional" : "Application is broken";
        AvailabilityChangeEvent.publish(this.applicationEventPublisher, state, updatedLivenessState);
        return state;
    }

    @PostMapping
    @RequestMapping("/readiness")
    public String updateReadinessState(){
        ReadinessState currentReadinessProbe = this.applicationAvailability.getReadinessState();
        ReadinessState updatedReadinessProbe = currentReadinessProbe == ACCEPTING_TRAFFIC ? REFUSING_TRAFFIC : ACCEPTING_TRAFFIC;
        String state = updatedReadinessProbe == ACCEPTING_TRAFFIC ? "Application is accepting traffice" : "Application is down";
        AvailabilityChangeEvent.publish(this.applicationEventPublisher, state, updatedReadinessProbe);
        return state;
    }
}
