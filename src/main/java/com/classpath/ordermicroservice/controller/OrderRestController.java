package com.classpath.ordermicroservice.controller;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/api/v1/orders")
public class OrderRestController {

    private final OrderService orderService;

    public OrderRestController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Order save(@RequestBody Order order){
        return this.orderService.saveOrder(order);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<Order> fetchAll(){
        return this.orderService.fetchAllOrders();
    }

    @GetMapping("/{id}")
    public Order fetchById(@PathVariable("id") long id){
        return this.orderService.findOrderById(id);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrderById(@PathVariable("id") long id){
        this.orderService.deleteOrderById(id);
    }
}
