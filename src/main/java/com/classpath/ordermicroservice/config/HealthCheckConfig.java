package com.classpath.ordermicroservice.config;

import com.classpath.ordermicroservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class HealthCheckConfig implements HealthIndicator {

    private final OrderRepository orderRepository;
    @Override
    public Health health() {
        try{
            long count = this.orderRepository.count();
            return Health.up().withDetail("DB-Service", "DB service is up").build();
        } catch (Exception exception) {
             return Health.up().withDetail("DB-Service", "DB service is down").build();
        }
    }
}
